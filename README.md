# Petgram
Petgram es una red sociial para mascotas, muestras publicaciones de animales por categorias.

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

_Tener instalado Node Js en tu ordenador, puedes obtener desde el sitio oficial para diferentes SOS._
* [Node js](https://nodejs.org/es/) - Entorno de ejecucion javascript phpMyAdmin como gestor de base de datos.

### Instalación 🔧

Abrir una terminal o linea de comandos apuntando a la raiz de la carpeta y ejecutar.

```
npm install
```
o
```
yarn install
```
_Despues que termine la instacion proceda a ejecutar el siguiente comando para correr la api en ambiente de desarrollo o local._

```
cd api/

yarn dev:api
```


## Despliegue 📦

_Configurado con vercel mediante **consola** cada vez que ejecute el comando **vercel** se realizara el deploy._

## Construido con 🛠️

_Para el desarrollo de esta api rest se utilizo las siguientes herramientas._

* [React](https://es.reactjs.org/) - React js, libreria para interfaz de usuario.
* [GraphQL](https://graphql.org/) - Capa de datos que comunica el cliente con el servidor.
* [Express](https://www.express.com/) - Framewok de JavaScript para servidores, uso multiproposito.
* [Jwt](https://jwt.io/) - Para autenticacion de usuarios con permisos y otros con ciertos permisos especificos.

## Autores ✒️

* **Andrés Coello** - *Developer full stack* - [Andres Coello](https://www.instagram.com/coellogoyes/)

## Licencia 📄

Este proyecto está bajo la Licencia (MIT)

## Expresiones de Gratitud 🎁

* Pasate por mi perfil para ver algun otro proyecto 📢
* Desarrollemos alguna app juntos, puedes escribirme en mis redes. 
* Muchas gracias por pasarte por este proyecto 🤓.


---
⌨️ con ❤️ por [Andres Coello](https://www.instagram.com/coellogoyes/) 😊

