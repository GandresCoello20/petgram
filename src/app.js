import React, { useContext, Suspense } from 'react'
import { Home } from './pages/home'
import { Detail } from './pages/Detail'
import { NavBar } from './components/NavBar'
import { User } from './pages/user'
import { NotRegisterUser } from './pages/notRegister'
// import { Favs } from './pages/favs'
import { GlobalStyle } from './components/GlobalStyle'
import { Logo } from './components/Logo/index'
import { Router, Redirect } from '@reach/router'
import { Context } from './Context'
import { NotFound } from './pages/notFound'

const Favs = React.lazy(() => import('./pages/favs'))

export function App () {
  const { isAuth } = useContext(Context)

  return (
    <Suspense fallback={<div />}>
      <GlobalStyle />
      <Logo />
      <Router>
        <NotFound default />
        <Home path='/' />
        <Home path='/pet/:id' />
        <Detail path='/detail/:detailId' />
        {!isAuth && <NotRegisterUser path='/login' />}
        {!isAuth && <Redirect noThrow from='/favs' to='/login' />}
        {!isAuth && <Redirect noThrow from='/user' to='/login' />}
        {isAuth && <Redirect from='/login' to='/' />}
        <User path='/user' />
        <Favs path='favs' />
      </Router>
      <NavBar />
    </Suspense>
  )
}
