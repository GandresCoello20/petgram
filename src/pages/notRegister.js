import React, { useContext } from 'react'
import { UserFOrm } from '../components/userForm'
import { Context } from '../Context'
import { LoginMutationm } from '../container/LoginMutation'
import { RegisterMutationm } from '../container/RegisterMutation'

export const NotRegisterUser = () => {
  const { activateAuth } = useContext(Context)
  return (
    <>
      <RegisterMutationm>
        {
          (register, { data, loading, error }) => {
            const onSubmit = ({ email, password }) => {
              const input = { email, password }
              const variables = { input }
              register({ variables }).then(response => {
                const { signup } = response.data
                activateAuth(signup)
              })
            }

            const errorMsg = error && 'El usuario ya existe'

            return <UserFOrm disabled={loading} error={errorMsg} onSubmit={onSubmit} title='Registrarse' />
          }
        }
      </RegisterMutationm>
      <LoginMutationm>
        {
          (login, { data, loading, error }) => {
            const onSubmit = ({ email, password }) => {
              const input = { email, password }
              const variables = { input }
              login({ variables }).then(response => {
                console.log(data)
                const { login } = response.data
                activateAuth(login)
              })
            }

            const errorMsg = error && 'El usuario no existe'

            return <UserFOrm error={errorMsg} disabled={loading} onSubmit={onSubmit} title='Iniciar Session' />
          }
        }
      </LoginMutationm>
    </>
  )
}
