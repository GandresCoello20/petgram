import React from 'react'
import { Helmet } from 'react-helmet'
import { ListOfPhotoCards } from '../container/ListOfPhotoCard'
import { ListOfCategories } from '../components/ListOfCategories/index'

const HomePage = ({ id }) => {
  return (
    <>
      <Helmet>
        <title>Petgram - tu app para mascotas</title>
        <meta name='description' content='Red social para mascotas' />
      </Helmet>
      <ListOfCategories />
      <ListOfPhotoCards categoryId={id} />
    </>
  )
}

export const Home = React.memo(HomePage, (prevProps, props) => {
  return prevProps.id === props.id
})
