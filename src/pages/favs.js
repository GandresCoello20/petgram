import React from 'react'
import { FavsWithQuery } from '../container/getFavorites'

export default () => {
  return (
    <>
      <h1>Favs</h1>
      <FavsWithQuery />
    </>
  )
}
