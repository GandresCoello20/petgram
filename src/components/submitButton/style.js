import Styled from 'styled-components'

export const Button = Styled.button`
    background: #8d00ff;
    border-radius: 3px;
    color: #fff;
    width: 100%;
    text-align: center;
    padding: 10px 5px;
    display: block;
    &[disabled]{
        opacity: 0.3;
    }
`