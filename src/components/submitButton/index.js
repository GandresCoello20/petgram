import React from 'react'
import { Button } from './style'

export const SubmitButton = ({ disabled, children }) => {
  return <Button disabled={disabled} onClick={children}>{children}</Button>
}
