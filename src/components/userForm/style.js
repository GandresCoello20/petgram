import Styled from 'styled-components'

export const Title = Styled.h2`
    font-size: 16px;
    font-weight: 500;
    padding: 8px 0;
`

export const Form = Styled.form`
    padding: 16px 0;
`

export const Input = Styled.input`
    border: 1px solid #ccc;
    border-radius: 3px;
    margin-bottom: 8px;
    padding: 8px 4px;
    display: block;
    width: 100%;
    &[disabled]{
        opacity: 0.3;
    }
`
export const Button = Styled.button`
    background: #8d00ff;
    border-radius: 3px;
    color: #fff;
    width: 100%;
    text-align: center;
    padding: 10px 5px;
    display: block;
    &[disabled]{
        opacity: 0.3;
    }
`
export const Error = Styled.span`
    font-size: 14px;
    color: red;
`
