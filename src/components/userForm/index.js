import React from 'react'
import { Form, Button, Input, Title, Error } from './style'
import { userInputValue } from '../../hooks/useInputValue'

export const UserFOrm = ({ onSubmit, title, error, disabled }) => {
  const email = userInputValue('')
  const password = userInputValue('')

  const handleSubmit = (event) => {
    event.preventDefault()
    onSubmit({ email: email.value, password: password.value })
  }

  return (
    <>
      <Title>{title}</Title>
      <Form onSubmit={handleSubmit}>
        <Input disabled={disabled} placeholder='Email' type='email' {...email} />
        <Input disabled={disabled} placeholder='Password' type='password' {...password} />
        <Button disabled={disabled}>{title}</Button>
      </Form>
      {error && <Error>{error}</Error>}
    </>
  )
}
