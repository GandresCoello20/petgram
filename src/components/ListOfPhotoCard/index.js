import React from 'react'
import { PhotoCard } from '../photoCard/index'

export const ListOfPhotoCardsComponent = ({ data: { photos = [] } }) => {
    return(
        <>
            {photos.map(photo => <PhotoCard key={photo.id} {...photo} />)}
        </>
    )
}

