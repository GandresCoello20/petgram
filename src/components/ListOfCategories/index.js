import React, { useState, useEffect } from 'react'
import { Categori } from '../categoria/index'
import { List, Item } from './styles'

function useCategoriData(){

    const [categories, setCtegories] = useState([]);
    const [laoding, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        window.fetch('http://localhost:3500/categories')
            .then(res => res.json())
            .then(response => {
                console.log(response);
                setCtegories(response)
                setLoading(false);
            })
    }, []);

    return {categories, laoding};
}

export const ListOfCategories = () => {
    const {categories, laoding} = useCategoriData();
    const [showFixed, setShowFixed] = useState(null);

    useEffect( () => {
        const onScroll = e => {
            const newShowFixed = window.scrollY > 200;
            showFixed != newShowFixed && setShowFixed(newShowFixed);
        }

        document.addEventListener('scroll', onScroll);
        return () => document.removeEventListener('scroll', onScroll);
    })

    const renderList = (fixed) => (
        <List fixed={fixed}>
            {laoding ? <Item key='load'>
                <Categori />
            </Item> : categories.map(category => <Item key={category.id}><Categori {...category} path={`/pet/${category.id}`} /></Item>)}
        </List>
    )

    if(laoding) {
        return 'cargando....';
    }

    return(
        <>
            {renderList()}
            {showFixed && renderList(true)}
        </>
    )
}